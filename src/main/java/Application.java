import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Application {

    List<String> list = new ArrayList<>();

    public void run() {
        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {
                String input = scanner.nextLine();
                if (input.isEmpty()) break;
                list.add(input);
            }
        }

        printStats();
    }

    private void printStats() {
        System.out.println("Number of unique words:");
        System.out.println(list.stream()
                .distinct()
                .count());
        System.out.println("Sorted list of all unique words:");
        System.out.println(list.stream()
                .distinct()
                .sorted()
                .collect(Collectors.toList()));
        System.out.println("Word count. Occurrence number of each word in the text:");
        System.out.println(list.stream()
                .collect(Collectors.groupingBy(i -> i, Collectors.counting())));
        System.out.println("Occurrence number of each symbol except upper case characters:");
        System.out.println(list.stream()
                .flatMap(i -> i.chars().boxed())
                .map(i -> (char) i.intValue())
                .collect(Collectors.groupingBy(i -> i, Collectors.counting())));
    }

    public static void main(String[] args) {
        new Application().run();

    }
}
