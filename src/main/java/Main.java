import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Main {

    /**
     * Lambda expression which calls a method <code>Math.max()</code> of three numbers.
     *
     * @see Math#max(int, int);
     */
    ThreeIntFunction maxValue = (a, b, c) -> Math.max(Math.max(a, b), c);
    /**
     * Analog of <code>maxValue</code> but this lambda exp is using <code>Stream</code>.
     */
    ThreeIntFunction maxValueStream = (a, b, c) -> IntStream.of(a, b, c).max().getAsInt();

    /**
     * This lambda exp returns a whole part of average of numbers.
     */
    ThreeIntFunction average = (a, b, c) -> (a + b + c) / 3;

    void task1Test() {
        System.out.println("Max value of (1, 2, 3): ");
        System.out.println(this.maxValue.method(1, 2, 3));

        System.out.println("Max value of (1, 2, 3) calculated with Stream: ");
        System.out.println(this.maxValueStream.method(1, 2, 3));

        System.out.println("Average of (1, 2, 4): ");
        System.out.println(this.average.method(1, 2, 4));
    }

    void task2Test() {
        Map<String, ICommand> map = new HashMap<>();

        map.put("hello", s1 -> System.out.println("Hello " + s1));
        map.put("beep", this::beep);
        map.put("print", new ICommand() {
            @Override
            public void execute(String s) {
                System.out.println("Your name is " + s);
            }
        });
        ICommand command = new ICommand() {
            @Override
            public void execute(String s) {
                System.out.println("Bye " + s);
                System.exit(0);
            }
        };
        map.put("exit", command);

        Scanner scanner = new Scanner(System.in);
        System.out.println("What is your name?");
        String name = null;
        if (scanner.hasNext()) {
            name = scanner.nextLine();
        }

        System.out.println("Available commands:");
        map.keySet().forEach(System.out::println);

        System.out.println("Choose one of them");

        while (true) {
            if (scanner.hasNext()) {
                String input = scanner.nextLine();
                ICommand comm = map.get(input);
                if (comm != null) {
                    comm.execute(name);
                }
            }
        }

    }


    private void beep(String name) {
        System.out.println("Beeping with " + name);
        Toolkit.getDefaultToolkit().beep();
    }

    public static void main(String[] args) {
        Main main = new Main();
//        main.task1Test();
//        main.task2Test();

        StreamTest streamTest = new StreamTest();
        streamTest.task3Test();
    }


}
