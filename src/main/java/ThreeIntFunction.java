@FunctionalInterface
public interface ThreeIntFunction {
    int method(int a, int b, int c);
}
