import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class StreamTest {

    public List<Integer> randomIntsWithIntStream(int count) {
        return IntStream
                .generate(ThreadLocalRandom.current()::nextInt)
                .limit(count)
                .boxed()
                .collect(Collectors.toList());
    }

    public List<Integer> randomIntsWithThreadLocalRandom(int count) {
        return ThreadLocalRandom
                .current()
                .ints(count)
                .boxed()
                .collect(Collectors.toList());
    }

    /**
     * WTF??
     */
    public Integer[] randomIntArrayFromDoubleStream(int count) {
        Integer[] ints = new Integer[count];
        return DoubleStream
                .generate(ThreadLocalRandom.current()::nextDouble)
                .limit(count)
                .mapToInt(i -> (int) i)
                .boxed()
                .collect(Collectors.toList())
                .toArray(ints);
    }

    public void task3Test() {
        List<Integer> list1 = randomIntsWithThreadLocalRandom(10);
        IntStream intStream = list1.stream().mapToInt(Integer::new);

        System.out.println("Generated ints:");
        System.out.println(list1);
        System.out.println("Statistics:");
        System.out.println(intStream.summaryStatistics());

        intStream = list1.stream().mapToInt(Integer::new);
        System.out.println("Sum of ints by sum method:");
        System.out.println(intStream.sum());

        intStream = list1.stream().mapToInt(Integer::new);
        System.out.println("Sum of ints by reduce:");
        System.out.println(intStream.reduce((a, b) -> a + b).getAsInt());

        intStream = list1.stream().mapToInt(Integer::new);
        int average = (int) intStream.average().getAsDouble();
        intStream = list1.stream().mapToInt(Integer::new).filter(i -> i > average);
        System.out.println("Average: " + average);
        System.out.println("Bigger than average: " + intStream.boxed().collect(Collectors.toList()));
    }
}
